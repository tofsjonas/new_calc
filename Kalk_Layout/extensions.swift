//
//  extensions.swift
//  Kalk_Layout
//
//  Created by Jonas Earendel on 2018-11-05.
//  Copyright © 2018 Ola Sandberg. All rights reserved.
//

import Foundation
//import UIKit
//
//
//extension UIButton {
//
//    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        isHighlighted = true
//        super.touchesBegan(touches, with: event)
//    }
//
//    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        isHighlighted = false
//        super.touchesEnded(touches, with: event)
//    }
//
//    override open func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        isHighlighted = false
//        super.touchesCancelled(touches, with: event)
//    }
//
//}

extension String {
    // https://stackoverflow.com/questions/27880650/swift-extract-regex-matches
    func matchingStrings(regex: String) -> [[String]] {
        guard let regex = try? NSRegularExpression(pattern: regex, options: []) else { return [] }
        let nsString = self as NSString
        let results  = regex.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))
        return results.map { result in
            (0..<result.numberOfRanges).map { result.range(at: $0).location != NSNotFound
                ? nsString.substring(with: result.range(at: $0))
                : ""
            }
        }
    }
    func escapeString() -> String {
        var newString = self.replacingOccurrences(of: "*", with: "\\*")
        newString = newString.replacingOccurrences(of: ".", with: "\\.")
        newString = newString.replacingOccurrences(of: "+", with: "\\+")
        return newString
    }

    func pregReplace(regex : String, replacement : String) -> String {
        let result = self.replacingOccurrences(of: regex, with: replacement, options: .regularExpression, range: self.startIndex ..< self.endIndex)
        return result
    }

}
extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
