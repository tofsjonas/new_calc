//
//  KalkTextField.swift
//  Kalk_Layout
//
//  Created by Jonas Earendel on 2018-11-05.
//  Copyright © 2018 Ola Sandberg. All rights reserved.
//

import UIKit
import Foundation
class KalkTextField: UITextField {

    var lastNumberRegex : String {
        return "([^0-9]{0,1}?)(-{0,1})([0-9]+(?:[\\.,][0-9]+)*)$"
    }

    func reversePolarity(){
        if self.text!.count <= 0 {return}
        let expression = self.text!

        let mathMatches = expression.matchingStrings(regex: lastNumberRegex)
        if mathMatches.count == 1 {
            var matches = mathMatches[0]

            if(matches[2] == "-"){//do the switch
                matches[2] = "+"
            }else{
                matches[2] = "-"
            }
            if(matches[1] == matches[2]){ // remove ++ and --
                matches[2] = ""
            }
            if(matches[2] == "+" && matches[1] != ""){//do the switch
                matches[2] = ""
            }

            var replacement = matches[1]+matches[2]+matches[3]
            replacement = replacement.replacingOccurrences(of: "+-", with: "-", options: .literal, range: nil)
            replacement = replacement.replacingOccurrences(of: "-+", with: "-", options: .literal, range: nil)
            let regex = matches[0].escapeString() + "$" // $ so it only replaces the last one in case there are more
            let result = expression.pregReplace(regex: regex, replacement: replacement)
            self.text = result
        }
    }
    func percentage(){
        if self.text!.count <= 0 {return}
        let expression = self.text!
        let mathMatches = expression.matchingStrings(regex: lastNumberRegex)
        if mathMatches.count == 1 {
            let matches = mathMatches[0]

            let new_number = Double(matches[3])! / 100
            let replacement = String(new_number.roundToDecimal(4)) // divide by 100, which is what it does
            let regex = matches[3].escapeString() + "$" // $ so it only replaces the last one in case there are more
            let result = expression.pregReplace(regex: regex, replacement: replacement)
            self.text = result
        }
    }
    func endsWithNumber() -> Bool{
        if self.text!.count <= 0 {return false}
        let expression = self.text!
        let mathMatches = expression.matchingStrings(regex: lastNumberRegex)
        return mathMatches.count == 1
    }


}


/*
 // Only override draw() if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 override func draw(_ rect: CGRect) {
 // Drawing code
 }
 */
