//
//  CALayer.shake.swift
//  Kalk_Layout
//
//  Created by Jonas Earendel on 2018-11-19.
//  Copyright © 2018 Ola Sandberg. All rights reserved.
//

import Foundation
import UIKit
extension CALayer {

    func shake(duration: TimeInterval = TimeInterval(0.5)) {

        let animationKey = "shake"
        removeAnimation(forKey: animationKey)

        let kAnimation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        kAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        kAnimation.duration = duration

        var needOffset = frame.width * 0.02,
        values = [CGFloat]()

        let minOffset = needOffset * 0.1

        repeat {

            values.append(-needOffset)
            values.append(needOffset)
            needOffset *= 0.5
        } while needOffset > minOffset

        values.append(0)
        kAnimation.values = values
        add(kAnimation, forKey: animationKey)
    }
}
