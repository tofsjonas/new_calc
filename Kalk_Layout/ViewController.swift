//
//  ViewController.swift
//  Kalk_Layout
//
//  Created by Ola Sandberg on 2018-09-24.
//  Copyright © 2018 Ola Sandberg, Gonza & Big J. All rights reserved.
//

import UIKit
import AudioToolbox


class ViewController: UIViewController {
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var primaryTextField: KalkTextField!
    @IBOutlet weak var equalsButton: UIButton!
    
    @IBOutlet weak var zeroButton: UIButton!

    //noteviwtext .scrollRangeToVisible(bottom)

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.primaryTextField.text = ""
        //self.primaryTextField.text = "45*5+45.67"
        primaryTextField.inputView = UIView()

        self.noteTextView.text = ""
        
        
        self.noteTextView.layoutManager.allowsNonContiguousLayout = false

        let down = UISwipeGestureRecognizer(target : self, action : #selector(ViewController.downSwipe))
        down.direction = .down
        self.view.addGestureRecognizer(down)

    }
    

    @IBAction func plusMinusClick(_ sender: UIButton) {
        if(!self.primaryTextField.endsWithNumber()){animateButtonError(sender);return}
        self.primaryTextField.reversePolarity()
        animateButton(sender)
//        updateEqualsButtonStatus()
    }
    @IBAction func percentageClick(_ sender: UIButton) {
        if(!self.primaryTextField.endsWithNumber()){animateButtonError(sender);return}
        self.primaryTextField.percentage()
//        updateEqualsButtonStatus()
        animateButton(sender)
    }
    @IBAction func eraseClick(_ sender: UIButton) {
        guard let txt = self.primaryTextField?.text else {animateButtonError(sender);return}
        if (txt.count == 0) {animateButtonError(sender);return}
        let indexEndOfText = txt.index(txt.endIndex, offsetBy: -1)
        self.primaryTextField.text = String(txt[..<indexEndOfText])

//        updateEqualsButtonStatus()
        animateButton(sender)
    }
    
    /*
    func updateEqualsButtonStatus(){
        let text = self.primaryTextField.text!
        if Kalk.isValidExpression(expression: text) {
            equalsButton.backgroundColor = UIColor.purple
        }else{
            equalsButton.backgroundColor = UIColor.yellow
        }
    }
    */

    @IBAction func equalsClick(_ sender: UIButton) {
        
        
        
        guard let textString = self.primaryTextField?.text else {animateButtonError(sender);return}
        guard Kalk.isValidExpression(expression: textString) else {animateButtonError(sender); return}
        let mathString = Kalk.doCalculation(expression: textString)
        if(mathString.count == 0){animateButtonError(sender);return}
        guard let mathValue = Double(mathString) else {animateButtonError(sender); return}
        let roundedNumber = mathValue.roundToDecimal(4)
        noteTextView.text.append("\n")
        noteTextView.text.append(textString)
        noteTextView.text.append("\n= ")
        noteTextView.text.append(roundedNumber.clean)
        primaryTextField.text = roundedNumber.clean
        animateButton(sender)
        
//        let stringLength:Int = self.noteTextView.text.count
        //Scroll to bottom
        self.noteTextView.scrollRangeToVisible(NSMakeRange(-1, 0))
    }
    @IBAction func acClick(_ sender: UIButton) {
        animateButton(sender)
        primaryTextField?.text = ""
//        updateEqualsButtonStatus()
    }
    @IBAction func numberClick(_ sender: UIButton) {
//        var button = sender
//        if let title = sender.titleLabel?.text {
//            if (title == "0" && button != self.zeroButton){
//
//                self.zeroButton.sendActions(for: .touchDown)
//                self.zeroButton.sendActions(for: .touchUpInside)
////                button.sendActions(for: .touchUpInside)
//                return
//
////                self.zeroButton.
//            }
//        }


        animateButton(sender)
        append(sender)
//        updateEqualsButtonStatus()
    }
    @IBAction func mathClick(_ sender: UIButton) {
        animateButton(sender)
        self.append(sender)
//        updateEqualsButtonStatus()
    }
    // TODO lägg till nolla om tomt fält ?
    @IBAction func commaClick(_ sender: UIButton) {
//        if !Kalk.lastCharIsNumber(text:self.primaryTextField.text ?? "") {animateButtonError(sender);return}
        animateButton(sender)
        self.append(sender)
//        updateEqualsButtonStatus()
    }
    func append(_ sender: UIButton) {
        self.primaryTextField.text?.append(sender.titleLabel!.text!)
    }

    // does nothing anymore....
    func animateButton(_ sender: UIButton) {

//        var button = sender
//        if let title = sender.titleLabel?.text {
//            if (title == "0"){
//                //detta funkar men ger inga resultat?
//                button = self.zeroButton
//            }
//        }
//        button.layer.layoutIfNeeded()
//        UIButton.animate(withDuration: 0.7, animations: {
//            button.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
//            button.layer.layoutIfNeeded()
//            self.view.layoutIfNeeded()
//
//        }, completion: { finish in
//            UIButton.animate(withDuration: 0.7, animations: {
//                button.transform = CGAffineTransform.identity
//                button.layer.layoutIfNeeded()
//                self.view.layoutIfNeeded()
//
//            })
//        })
    }
    func animateButtonError(_ sender: UIButton) {
        self.primaryTextField.layer.shake(duration: 0.2)
        AudioServicesPlaySystemSound(1520)
    }
    
    // Share
    @IBAction func share(_ sender: Any) {
//        let message = noteTextView.text
//            let objectsToShare = [message,link]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]
//            self.presentViewController(activityVC, animated: true, completion: nil)
    }

}

//let selectedRange: UITextRange? =  self.primaryTextField.selectedTextRange
//print(selectedRange)
//if let selectedRange = primaryTextField.selectedTextRange {
//
//    let cursorPosition = primaryTextField.offset(from: primaryTextField.beginningOfDocument, to: selectedRange.start)
//
//    print("\(cursorPosition)")
//}
