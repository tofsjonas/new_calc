//
//  Kalk.swift
//  Kalk_Layout
//
//  Created by Jonas Earendel on 2018-10-07.
//  Copyright © 2018 Ola Sandberg. All rights reserved.
//

import Foundation
import SwiftTryCatch

class Kalk {
    static func convertExpressionToDouble(expression : String) -> String {
        let mathMatches = expression.matchingStrings(regex: "([^0-9]+)*([0-9]+([\\.,][0-9]+)*)([^0-9]+)*")
        var mathString = ""
        for match in mathMatches {
            mathString.append(match[1]) // -+/( osv
            mathString.append(match[2]) // talet, dvs 12, 135, 67 osv
            if(match[3].count==0){      // om ingen decimaldel, lägg till .0
                mathString.append(".0")
            }
            mathString.append(match[4]) // trailing -+/() osv
        }
        return mathString
    }
    static func isValidMath(text : String) -> Bool{
        return false;
    }
    static func doCalculation(expression : String) -> String {
        let mathString = convertExpressionToDouble(expression: expression)
        return Kalk.calculate(expression: mathString)
    }
    static func getLastCharacter(txt:String) -> String {
        if (txt.count == 0) {return ""}
        let indexStartOfText = txt.index(txt.startIndex, offsetBy: txt.count-1)
        return String(txt[indexStartOfText...])
    }

    static func isValidExpression(expression : String) -> Bool {
        return Kalk.calculate(expression: expression).count > 0
    }

    private static func calculate(expression : String) -> String {
        var textString = expression.replacingOccurrences(of: "x", with: "*", options: .literal, range: nil)
        textString = textString.replacingOccurrences(of: ",", with: ".", options: .literal, range: nil)
        textString = textString.replacingOccurrences(of: "–", with: "-", options: .literal, range: nil)
        textString = textString.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
        var resultString  = ""
        SwiftTryCatch.try({
            let predicate = NSPredicate(format: "1.0 * \(textString) = 0")
            if let comparisation = predicate as? NSComparisonPredicate {
                let leftExpression = comparisation.leftExpression
                if let result = leftExpression.expressionValue(
                    with: nil, context: nil) as? NSNumber
                {
                    resultString = String(result.doubleValue)
                }
            }
        }, catch: { (error) in
        }) {}
        return resultString
    }

}
