//
//  ViewController.downSwipe.swift
//  Kalk_Layout
//
//  Created by Jonas Earendel on 2018-11-19.
//  Copyright © 2018 Ola Sandberg. All rights reserved.
//

import Foundation
import UIKit
extension ViewController {
    @objc
    func downSwipe(){
        view.endEditing(true)
    }
}
